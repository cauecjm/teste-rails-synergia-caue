### Cauê J. Martinez - Teste para Synergia ###

Deixei uma versão funcionando em produção no Heroku e abaixo segue link para visualizar o funcionamento do app.
Obs:
- estou utilizando FreeDyno, onde a máquina é desligada automaticamente quando está inoperante por 30 minutos. Então quando tentar acessar pode ser que apresente erro. A máquina iniciará e ficará disponível, então é necessário insistir no acesso.
- no Heroku não estou utilizando Docker, mas o app está totalmente configurado para Docker.

https://bitestoque.herokuapp.com/

Comentários:
- utilizei uma versão mais recente do Rails (5.2), pois a versão 4.2 é praticamente obsoleta. Encontra-se apenas em projetos legados.
- a gem 'factory_girl_rails' nem existe mais, agora é 'factory_bot'
- ao invés de 'sqlite3' utilizei 'postgres'. Alterei para poder colocar o app funcionando no Heroku sem custos com banco de dados, já que eles permitem uma versão gratuita no Postgres
- adicionei mais algumas gems, umas para auxiliar o próprio framework como 'min_racer' e 'responders', e outras para auxiliar nos testes usando 'rspec-rails'
- alterei a estrutura do banco de dados:
  - adicionei campos nas tabelas 'colors' e 'sizes' unicamente para melhorar a experiência do usuário.
  - já na tabela 'available_products' adicionei o campo 'qtd'. Analisando um futuro do app, do jeito que essa tabela foi apresentada, a tabela iria crescer e ficar com um volume de linhas gigantesco gerando lentidão nas querys. Com a alteração que fiz a tabela terá um volume de linhas absurdamente menor.
- montei um layout onde é possível realizar todas as funções solicitadas na mesma tela, com isso melhorando a experiência de utilização do usuário. Além disso configurei para que o layout seja responsivo, alguns elementos se posicionam de forma diferente para diferentes resoluções. Isso é possível testar no app que deixei em produção no Heroku.

### BITESTOQUE ###

App BitEstoque

Aplicativo para gerenciamento de estoque. Desenvolvido para fins de estudos de desenvolvimento (programação - dev).

## Ferramentas utilizadas:

- ruby 2.5.1
- rails 5.2.0
- postgres
- puma
- coffee rails
- jquery rails
- bootstrap
- rspec-rails
- ffaker
- factory bot rails
- database cleaner
- shoulda matchers
- rails-controller-testing

## Executado utilizando Docker:

- Dockerfile
- docker-compose version 3

## Execução de testes automatizados:

```
docker-compose run --rm app bundle exec rspec spec/*
```

## Execução:

```
docker-compose build

docker-compose run --rm app bundle install

docker-compose run --rm app bundle exec rails db:create db:migrate db:seed

docker-compose up
```
